/**
 * Read cube from cube.json
 * @author wangjian5@genomics.cn
 *
 */
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.json.*;

public class CubeUtil {

	
	/**
	 * 指定json文件地址，从中读取Cube对象.
	 * @param path
	 * @return LiCube object list. if error or path is invalid, return null;
	 */
	public static List<LiCube> getCubesFromJsonFile(String path)
	{
		String json = readFrom(path);
		System.out.println(json);
		if (json != null)
			return JsonToCube(json);
		else
			return null;
	}
	
	/**
	 * Read string[] from json file.
	 * @param path where is json file
	 * @return
	 * @throws  
	 */
	private static String readFrom(String path)
	{
		
		File fjson = new File(path);
		if (fjson.exists() && fjson.isFile()){
			char[] buff = new char[(int) fjson.length()];
			try {
				FileReader r = new FileReader(fjson);
				InputStreamReader fread =new InputStreamReader(new FileInputStream(fjson),"utf-8");
				fread.read(buff);
				fread.close();
				return new String(buff);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		else 
			return null;	
	}
	
	/**
	 * Get List<LiCube>
	 * @param objectstr
	 * @return
	 */
	private static List<LiCube> JsonToCube(String objectstr){
		JSONArray jsonArray = new JSONArray(objectstr);
		if (jsonArray.length() ==0)
			return null;
		//System.out.println(array);
		List<LiCube> cubes = new ArrayList<LiCube>(jsonArray.length());
		for(int i = 0; i < jsonArray.length(); i++){
			JSONObject o = jsonArray.getJSONObject(i);
			if ( o != null)
				cubes.add(new LiCube(o.getString("name")));
		}
		return cubes;		
	}
}
