import org.apache.commons.csv.*;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CubeConfDB {
	private final String confdb = "./data/cube.conf";
	
	/**
	 * 添加一条Cube监控配置信息.
	 * 记住！！该函数是严格串行化的。如果用在多线程环境，不会有写问题，但是会降低效率。
	 * @param name
	 * @param target
	 * @param source
	 * @param interval
	 * @return true if add successful.
	 */
	public synchronized boolean add(String name, String target, String source, String interval){
		BufferedWriter writer = getFileWriter(confdb);
		if (writer == null)
			return false;
		
		//else
		String newline = String.format("%s\t%s\t%s\t%s\t", name,target,source,interval);
		try {
			writer.append(newline);
			writer.flush();
			writer.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 以append = true打开文件，返回已经到文件结尾并换行的BufferedWriter instance
	 * Remeber close file writer
	 * @param fpath
	 * @return
	 */
	private BufferedWriter getFileWriter(String fpath){
		try {
			//FileWriter writer = new FileWriter(confdb);
			//InputStreamReader fread =new InputStreamReader(new FileInputStream(fjson),"utf-8");
			BufferedWriter dbw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fpath,true),"utf-8"));
			dbw.newLine();
			/*dbw.append("eac");
			dbw.newLine();
			dbw.append("eBc");
			dbw.flush();
			dbw.close();*/
			return dbw;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
